function Jugador(nombre){
    this.nombre=nombre;
    this.pv=100;
    this.sp=100;

    this.curar=function(jugadorObjetivo){
        if(this.sp>40){
            jugadorObjetivo.pv+=20;
            this.sp-=40;
        }else{
            console.log(this.nombre+ " no tiene suficientes puntos de magia");
        }
        this.estado(jugadorObjetivo)
        
    }

    this.atacar=function(jugadorObjetivo){
        if (jugadorObjetivo.pv>40){
            jugadorObjetivo.pv-=40;
        }else{
            console.info(jugadorObjetivo.nombre + " murió !!!");
        }
        
        this.estado(jugadorObjetivo);
    }

    this.estado=function(jugadorObjetivo){
        console.info(this);
        console.info(jugadorObjetivo);
    }
}

var gandalf=new Jugador("Gandalf");
var legolas=new Jugador ("Légolas");

gandalf.curar(legolas)
gandalf.curar(legolas)
gandalf.curar(legolas)
gandalf.curar(legolas)
gandalf.curar(legolas)
legolas.atacar(gandalf)
legolas.atacar(gandalf)
legolas.atacar(gandalf)
legolas.atacar(gandalf)
legolas.atacar(gandalf)
legolas.atacar(gandalf)
legolas.atacar(gandalf)
legolas.atacar(gandalf)
legolas.atacar(gandalf)
legolas.atacar(gandalf)
legolas.atacar(gandalf)
legolas.atacar(gandalf)
legolas.atacar(gandalf)
legolas.atacar(gandalf)
legolas.atacar(gandalf)
legolas.atacar(gandalf)
