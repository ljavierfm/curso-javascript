//5 tipos de datos primitivos:
var str="texto";
var bool=true;
var num=10;
var und=undefined;
var nul=null;

//objeto : coleccion de datos primitivos u otros objetos. Siempre par:valor
var obj={
    num:10,
    str:'text',
    childObj:{
        num:20,
        str:'text',
        bool:false
    }

    
}
console.log(obj);

/*valor y referencia*/

//primitivos por valor
var a=10;
var b=a;

console.info("a:",a);
console.info("b",b);
a=20;
console.info("a:",a);
console.info("b",b);

//objetos por referencia. Apuntan al mismo espacio de memoria.
var c={ nombre:'Juana'}
var d=c;
console.info("c:",c);//{ nombre:'Juana'}
console.info("d",d);//{ nombre:'Juana'}
c.nombre='Pedro';
console.info("c:",c);//{ nombre:'Pedro'}
console.info("d",d);//{ nombre:'Pedro'}

/*
Notación de puntos y corchetes
*/
var persona={
    nombre:'Juan',
    apellido:'Fernández',
    edad:25,
    direccion:{
        pais:'Bélgica',
        ciudad:'Brujas',
        edificio:{
            numero:22,
            puerta:"A"
        }
    }
}

console.log(persona.direccion.edificio.numero);
console.log(persona['direccion']['edificio']['numero']);

/*
Funciones
*/

function primeraFuncion(){
    var a=20;
    console.log(a);
}

//lamada a la función. LAS FUNCIONES SIEMPRE DEVUELVEN UN VALOR
primeraFuncion(); //retorna undefined
var miFuncion=primeraFuncion //Devuelve un objeto que es la declaración de primeraFuncion()
miFuncion() // 20

/*
Parámetros de las funciones:
Primitivos, objetos, funciones.
*/

function imprimir(nombre,apellido){
    //var apellido = apellido ||'XXX' -> Se evita el undefined si no llamamos con el parámetro "apellido"
    console.log(nombre+" "+ apellido);
}

imprimir("Juan"); //Juan undefined . Al declarar la función ya asigna apellido a undefined.

//pasar un objeto como parámetro
imprimir({
    nombre:'Juan',
    apellido:'Perez' //[object Object] undefined
})

function imprimirObjeto(persona){
    console.log(persona.nombre+ " "+persona.apellido );
}

imprimirObjeto({
    nombre:'Juan',
    apellido:'Perez'
}) //Juan Perez

var persona={
    nombre:'Juan',
    apellido:'Perez'
}
imprimirObjeto(persona);//Juan Perez

//funciones por parámetros
function imprimirFuncion(fn){
    fn();
}

imprimirFuncion(function(){
    console.log('Función anónima');
});//Función anónima

var miFuncion=function(){
    console.log('miFunción');
}
imprimirFuncion(miFuncion);//miFunción

/*
El retorno de las funciones
Primitivo,undefined,objetos,función
*/

//PRIMITIVOS
function obtenerAleatorio(){
    return Math.random();//aleatorio n>=0 y n<1
}
console.log(obtenerAleatorio()+10);

function obtenerNombre(){
    return "Juan";
}
console.log(obtenerNombre()+ "Gutierrez"); //Juan Gutierrez

var nombre=obtenerNombre();//Juan

function esMayor05(){
    if( obtenerAleatorio()>0.5){
        return true;
    }else{
        return false;
    }
}
console.log(esMayor05());// true || false (depende del aleatorio que genere)

if(esMayor05()){
    console.log('Es mayor a 0.5');
}else{
    console.log('Es menor a 0.5');
} // Es mayor a 0.5 || Es menor a 0.5 (depende del aleatorio que se genere con esMayor05())

//UNDEFINED
function ejemploUndefined(numero){
    if (numero%2==0){
        return 'es par';
    }
}
console.log(ejemploUndefined(1));//undefined

//OBJETOS
function crearPersonal(nombre,apellido){
    return {
        nombre:nombre,
        apellido:apellido
    }
}

var persona=crearPersonal('Javier','Fernández');
console.log(persona);//Object { nombre: "Javier", apellido: "Fernández" }
console.log(persona.nombre);//Javier

//FUNCIONES
function crearFuncion(){
    return function(){
        console.log('Me crearon!!!');

        return function(){
            console.log('Segunda función!!!');
        }
    }
}

var nuevaFuncion=crearFuncion();
var nuevaNuevaFuncion=nuevaFuncion();//Me crearon!!!
nuevaNuevaFuncion();//Segunda función!!!

/*
Funciones de primera clase. Las funciones son objetos
*/

function x(){
       console.log('Función X'); 
}
x();//Functión A
x.nombre='Pepe';
x.direccion={
    calle:'Rayo',
    numero:22,
    piso:{
        edificio:'Buenavista',
        Portal : 3
    }
}

/*
Metodos y el objeto THIS
*/

var persona={
    nombre:'Maria',
    apellido:'Dubon',
    imprimirNombre: function(){
        //this hace referencia al contexto donde se lanza el método, en este caso "persona"
        console.log(this.nombre+" "+this.apellido);
        console.log(this);//{nombre: 'Maria', apellido: 'Dubon', direccion: {…}, imprimirNombre: ƒ}
    },
    direccion:{
        pais:'España',
        obtenerPais:function (){
            console.log(this);//{pais: 'España', obtenerPais: ƒ}
            console.log('La dirección es '+this.pais);

            //apunta al objeto que lo contiene, por referencia ya que no es un primitivo.
            var self=this;//{pais: 'España', obtenerPais: ƒ}
            var nuevaDireccion=function(){
                console.log(self.pais);//
            }

            nuevaDireccion()//España
        }
    }
}
persona.imprimirNombre();//Nombre completo
persona.direccion.obtenerPais();//La direccion es España

/*
La palabra reservada NEW
*/

var juan={}
console.log(juan);//{}

function Persona(){
    this.nombre="Javier";
    this.apellido="Fernandez";
    this.edad=44;
    this.imprimirPersona=function(){
        return this.nombre+" "+this.apellido+" ("+this.edad+") años";
    }
}

juan=Persona()
console.log(juan);//undefined (logico, Persona() devuelve un undefined)
//con new creas un objeto de Persona. Es similar a las clases de Java
juan=new Persona()
console.log(juan);//Persona{}
console.log(juan.imprimirPersona());//Javier Fernandez (44) años

/*
PROTOTIPOS
*/

Persona.prototype.pais='España';
console.log(juan);//Object { nombre: "Javier", apellido: "Fernandez", edad: 44, imprimirPersona: imprimirPersona() }

/*
FUNCIONES ANÓNIMAS
*/

var a=10;

function cambiarA(){
    a=20;
}
console.log(a);//10
cambiarA(a);
console.log(a);//10

/*
TYPEOF
*/
function identifica(param){
    console.log(typeof param);
}

function Personaje(){
    this.nombre="javier"
}

/**
 * ARREGLOS
 * Son dinámicos y pueden crecer
 */

var arr=[5,4,3,2,1]
console.log(arr);//[ 5, 4, 3, 2, 1 ]

//posiciones, empieza en 0
console.log(arr[0]);//5
console.log(arr[5]);//undefined

// reverse() cambia el orden del array
arr.reverse(); //al estar cambiado por referencia no genera un nuevo objeto
console.log(arr);// [ 1, 2, 3, 4, 5 ]

// map() ejecuta una función contra cada 1 de los elementos del array, recibe como param cada elemento del array
arr=arr.map(function(elem){
    return elem*=2;
})
console.log(arr);// [ 2, 4, 6, 8, 10 ]

//join() une los elementos en una cadena, se pueden separar por parámetros
arr=arr.join("|");
console.log(arr);// 2/4/6/8/10

//split() corta una cadena en un array de cadenas, usando el separador indicado
arr=arr.split("|",3)
console.log(arr);// Array(3) [ "2", "4", "6" ]

//push() y unshift() Añaden elementos al final o al principip del Arreglo
arr.push(6);
console.log(arr);// Array(4) [ "2", "4", "6", 6 ]
arr.unshift(0);
console.log(arr);// Array(5) [ 0, "2", "4", "6", 6 ]

//toString() muestra la representación en string
arr=arr.toString();
console.log(arr); //0,2,4,6,6
arr=arr.split(",");
console.log(arr);//Array [ "0,2,4,6,6" ]

//pop() elementa el último elemento del arreglo, devolviendo el último elemento eliminado.
eliminado=arr.pop();
console.log(eliminado);//6
console.log(arr);//Array(4) [ "0", "2", "4", "6" ]

//splice() cambia el contenido de un array eliminando elementos existentes y/o agregando nuevos elementos.
//array.splice(start[, deleteCount[, item1[, item2[, ...]]]])
arr.splice(1,2,"nuevo","mas nuevo")
console.log(arr);// Array(4) [ "0", "nuevo", "mas nuevo", "6" ]
arr.splice(0,0,"otro nuevo");
console.log(arr); // Array(5) [ "otro nuevo", "0", "nuevo", "mas nuevo", "6" ]

//splice() devuelve una copia de una parte del array dentro de un nuevo array 
// empezando por inicio hasta fin (fin no incluido). El array original no se modificará.
//arr.slice([inicio [, fin]])
arr2=arr.slice(0,2);
console.log(arr);// Array(5) [ "otro nuevo", "0", "nuevo", "mas nuevo", "6" ]
console.log(arr2);// Array [ "otro nuevo", "0" ]

//length indica cuantos elementos tiene un array
console.log(arr2.lenght); //2

var arrVarios=[
    true,
    {
        nombre:'Javier',
        apellido:'Fernández'
    },
    function(){
        console.log('Función desde array');
    },
    function(persona){
        console.log(persona.nombre+' '+persona.apellido);
    }
]

arrVarios[3](arrVarios[1]) // Javier Fernández

/*
ARGUMENTS
Están en el prototipo de la función
*/

function miFuncionArguments(){
    console.log(arguments);//[]
}

/*
COOKIES
*/
function crearCookie(nombre,valor){
    valor=encodeURIComponent (valor);
    expDate=new Date();
    expDate.setMonth(expDate.getMonth()+1);
    document.cookie=nombre+"="+valor+";expires="+expDate.toUTCString()+";SameSite=none;";
}
crearCookie("nombre","javier Pérez");